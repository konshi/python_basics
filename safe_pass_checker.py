
def checker(new_pass):
    num_rule = '1234567890'
    str_rule = 'abcdefghijklmnopqrstuvwxyz'
    upper_str_rule = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    char_rule = '!@#$%^&*()-+'
    bad_pass = 'Слабый пароль. Рекомендации: '
    count1 = count2 = count3 = count4 = 0
    for n in num_rule:
        if n in new_pass:
            count1 += 1
    for s in str_rule:
        if s in new_pass:
            count2 += 1
    for u in upper_str_rule:
        if u in new_pass:
            count3 += 1
    for ch in char_rule:
        if ch in new_pass:
            count4 += 1
# проверяем результат качества пароля
    if len(new_pass) < 12:
        bad_pass += f'увеличить число символов: + {12 - len(new_pass)}, '
    if count1 == 0:
        bad_pass += 'добавить 1 цифру, '
    if count2 == 0:
        bad_pass += 'добавить 1 строчную букву, '
    if count3 == 0:
        bad_pass += 'добавить 1 заглавную букву, '
    if count4 == 0:
        bad_pass += 'добавить 1 спецсимвол, '
    for i in new_pass:
        if i not in (num_rule + str_rule + upper_str_rule + char_rule):
            bad_pass = 'Ошибка. Запрещенный спецсимвол'
            return bad_pass
    if count1 > 0 and count2 > 0 and count3 > 0 and count4 > 0 and len(new_pass) >= 12:
        return 'Сильный пароль.'
    return bad_pass.rstrip(', ')

# new_pass = input('Придумайте новый пароль\n')
# print(checker(new_pass))
