# import re
# print(re.match(r'Hey', 'Hey Hey').group(0))
# print(re.match(r'Hey', 'HeyLo Hey').group(0))
# print(re.search(r'\w', 'hey Hey Hey Hey').group(0))  # Добавляем метод group(), чтобы вывести содержимое поиска
# print(re.findall(r'h..', 'hey Hey Hey Hey'))
# print(re.findall(r'h+', 'hey heyhhh hey Heyh'))
# print(re.findall(r'H.+', 'hey Hey Hey Hey'))
# print(re.findall(r'\w', 'hey Hey Hey Hey'))
# print(re.findall(r'h|3', 'hey333. hey1. Hey2. Hey3'))
import re
logfile = open('logfile.txt', 'r')
for string in logfile:
    if re.findall(r'Did', string):
        print(re.findall(r'\d+\.\d+\.\d+\.\d+', string))
