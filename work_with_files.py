task_file = open('task_file.txt', 'r')
task_file_reads = task_file.readlines()

def email_gen(list_of_names):
    emails = []
    for i in list_of_names:
        letter = 1
        while i[1] + '.' + i[0][0:letter] + '@company.io' in emails:
            letter+=1
        emails.append(i[1] + '.' + i[0][0:letter] + '@company.io')
    return emails

list_bio = []
e_list = []

for line in task_file_reads:
    line = line.strip().split(', ')
    list_bio.append(line)

task_file.close()

for l in list_bio[1:]:
    name, last_name, number = l[1], l[2], l[3]
    if len(name) == 0 or len(last_name) == 0 or len(number) != 7 or number.isdigit() == False or (name[0].islower() or last_name[0].islower()) or name.isalpha() != True:
        continue
    else:
        e_list.append(email_gen([[name, last_name]]))
        l[0] = ''.join(e_list[-1])

print(list_bio)
genetate_list = open('genetate_file.txt', 'w')

for i in list_bio:
    genetate_list.write(', '.join(i) + '\n')

genetate_list.close()
