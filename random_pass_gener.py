import random
from safe_pass_checker import checker

new_pass_length = input('Введите число - количество символов из которых должен состоять пароль. Не менее 12.\n')

num_rule = '1234567890'
str_rule = 'abcdefghijklmnopqrstuvwxyz'
upper_str_rule = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
char_rule = '!@#$%^&*()-+'
all_char = num_rule + str_rule + upper_str_rule + char_rule
new_random_pass = ''

def re_generate(pass_length):
    new_random_pass = ''
    for i in range(int(pass_length)):
        new_random_pass += random.choice(all_char)
    if checker(new_random_pass) == 'Сильный пароль.':
        return new_random_pass
    else: re_generate(pass_length)

print(re_generate(new_pass_length))
